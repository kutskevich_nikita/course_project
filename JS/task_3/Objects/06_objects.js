/*6. Write a function that groups an array of persons by age, first or last name.
    The function must return an associative array, with keys - the groups, and
values -arrays with persons in this groups. Use function overloading (i.e. just one function).*/

const group = (array, fn) => {
    if(fn === 'age'){
       array =  array.map(val => val.age)
            .reduce((acc, val, i) => {
                acc[val] = (acc[val] || []).concat(array[i]);
                return acc;
            }, {});
    } else if(fn === 'firstname') {
        array =  array.map(val => val.firstName)
            .reduce((acc, val, i) => {
                acc[val] = (acc[val] || []).concat(array[i]);
                return acc;
            }, {});
    } else if(fn === 'lastname') {
        array =  array.map(val => val.lastName)
            .reduce((acc, val, i) => {
                acc[val] = (acc[val] || []).concat(array[i]);
                return acc;
            }, {});
    }
    return array;
}

const personsSix = [
    { firstName : "Gosho", lastName: "Petrov", age: 32 },
    { firstName : "Bay", lastName: "Ivan", age: 81 },
    { firstName : "Bay", lastName: "Vezovik", age: 32 },
    { firstName : "Vladislav", lastName: "Volchek", age: 81 },
    { firstName : "Georgiy", lastName: "Volchek", age: 27 },
    { firstName : "Vladislav", lastName: "Petrov", age: 20 }
];

console.log(`06_objects: ${JSON.stringify(group(personsSix, 'firstname'))}`);