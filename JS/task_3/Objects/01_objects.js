/*1. Write functions for working with shapes in standard Planar coordinate system
* Points are represented by coordinates `P(X, Y)`
* Lines are represented by two points, marking their beginning and ending: `L(P1(X1,Y1)`, `P2(X2,Y2))`
* Calculate the distance between two points
* Check if three segment lines can form a triangle*/


const points = {
    firstPoint:{
        x: 1,
        y: 1
    },
    secondPoint:{
        x: 4,
        y: 5
    },
    thirdPoint:{
        x: 5,
        y: 1
    }
};

const constructTriangle = (obj) => {

    const firstLine = Math.sqrt(Math.pow(obj.firstPoint.x  - obj.secondPoint.x, 2)
        + Math.pow(obj.firstPoint.y  - obj.secondPoint.y, 2));
    const secondLine = Math.sqrt(Math.pow(obj.secondPoint.x  - obj.thirdPoint.x, 2)
        + Math.pow(obj.secondPoint.y  - obj.thirdPoint.y, 2));
    const thirdLine = Math.sqrt(Math.pow(obj.firstPoint.x  - obj.thirdPoint.x, 2)
        + Math.pow(obj.firstPoint.y  - obj.thirdPoint.y, 2));

    if(firstLine > secondLine + thirdLine || secondLine > firstLine + thirdLine ||
        thirdLine > secondLine + firstLine){
        return {
            firstLine: firstLine,
            secondLine: secondLine,
            thirdLine: thirdLine,
            canForm: false
        }
    }
    return {
        firstLine: firstLine,
        secondLine: secondLine,
        thirdLine: thirdLine,
        canForm: true
    }
}

const result = constructTriangle(points);

console.log(`01_objects:\nDistance between points are ${result.firstLine}, ${result.secondLine}, ${result.thirdLine}
Is it possible to build a triangle with these lines? - ${result.canForm ? 'Yes' : 'NO'}`);
