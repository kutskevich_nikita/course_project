/*Write a function that checks if a given object contains a given property*/

const hasProperty = (obj, value) => {
    return ({}).hasOwnProperty.call(obj, value);
}

const newCar = {
    hasOwnProperty: function(){
        return false;
    },
    make: 'Porsche',
    model: '911'
}

const property = 'model';
const hasProp = hasProperty(newCar, property);

console.log(`04_objects:\nobject: ${JSON.stringify(newCar)}; property: ${property}; result: ${hasProp}`);