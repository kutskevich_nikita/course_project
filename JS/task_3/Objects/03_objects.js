/*3. Write a function that makes a deep copy of an object. The function
should work for both primitive and reference types.*/

const createDeepCopy = (obj) => {
    const newObj = {};
    for(let key in obj){
        if(obj[key] instanceof Object){
            newObj[key] = createDeepCopy(obj[key]);
        } else {
            newObj[key] = obj[key];
        }
    }
    return newObj;
}

const car = {
    make: 'Porsche',
    model: '911',
    characteristics:{
        power: 385,
        acceleration: 4.2,
        topSpeed: 293
    }
}

const cloneCar = createDeepCopy(car);
console.log(`03_objects:\nCloned object: ${JSON.stringify(cloneCar)}`);
