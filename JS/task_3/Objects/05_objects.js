/*5. Write a function that finds the youngest person in a given array of persons and prints his/hers full name
* Each person has properties `firstName`, `lastName` and `age`*/

const persons = [
    { firstName : "Gosho", lastName: "Petrov", age: 32 },
    { firstName : "Bay", lastName: "Ivan", age: 81 },
    { firstName : "Ilya", lastName: "Vezovik", age: 15 },
    { firstName : "Vladislav", lastName: "Volchek", age: 40 },
    { firstName : "Georgiy", lastName: "Chevchan", age: 27 }
];

const getYoungestPerson = (array) => {
    let minAge = array[0].age;
    let minKey = 0;
    for(const key in array){
        if(array[key].age < minAge){
            minAge = array[key].age;
            minKey = key;
        }
    }
    console.log(`05_objects: The youngest person is ${array[minKey].firstName} ${array[minKey].lastName}`);
}

getYoungestPerson(persons);