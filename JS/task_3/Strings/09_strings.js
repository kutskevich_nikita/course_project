/*9. Write a function for extracting all email addresses from given text.
    All substrings that match the format `<identifier>@<host>...<domain>`
should be recognized as emails. Return the emails as array of strings.*/


const findEmails = (text) =>{
    const result = text.matchAll(/(?<identifer>[_.\S\w\d]+)@(?<host>[.\S\w]+)(?<domain>\.[\S\w]+)/g);
    const array = [];
    for(const key of result){
        array.push(key[0]);
    }
    return array;
}

const textNine = 'Christmas is one of the arkaman@mail.ru most popular holidays on the planet. ' +
    'It seems strange but not everybody bolobob@outlook.com celebrate михаилЖитников@яндекс.ру such a great holiday. It depends on the religion and the culture of people. ' +
    'So, who does not janet.kusov@yan.dex.net celebrate this holiday?';

const arrayOfStrings = findEmails(textNine);
console.log(`09_strings: result - ${arrayOfStrings}`);

