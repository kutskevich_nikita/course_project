/*3. Write a JavaScript function that finds how many times a substring is
contained in a given text (perform case insensitive search).*/

const checkSubstring = (str, substr) => {
    const regexp = new RegExp(substr, 'gi');
    return str.match(regexp).length;
}

const sourceStr = 'We are living IN an yellow submarine.';
const substr = 'in';
console.log(`03_strings: string: ${sourceStr}; substring: ${substr}; result count = ${checkSubstring(sourceStr, substr)}`);
