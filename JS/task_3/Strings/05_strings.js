/*Write a function that replaces non breaking white-spaces in a text with `&nbsp;`*/

const replaceSpaces = (str) => {
    return str.replace(/[ ]/g, '&nbsp');
}

const initialStr = 'Hel lo W orld';
const divForStr = document.getElementById('05-strings');

const replacedStr = replaceSpaces(initialStr);

console.log(`05_strings: initial string: ${initialStr}; result: ${replacedStr}`);
divForStr.innerHTML = `<p>05_strings: ${replacedStr}</p>`;