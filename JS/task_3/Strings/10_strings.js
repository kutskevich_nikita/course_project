/*10. Write a program that extracts from a given text all palindromes, e.g. "`ABBA`", "`lamal`" "`exe`".*/

const getPalindromes = (str) => {
    str = str.split(/[.,\s!?]+/gi);
    const array = [];

    for(const key in str){
        if(str[key] === str[key].split('').reverse().join('') && str[key].length > 2){
            array.push(str[key]);
        }
    }
    return array.join(', ');
}

const textTen = 'Christmas is one of ABBA the most popular holidays lamal on the planet. ' +
    'It seems strange but not exe everybody celebrate such a great holiday. It depends on the deed religion and the culture level of people. ' +
    'So, who does not noon celebrate this holiday?';

const palindromes = getPalindromes(textTen);
console.log(`10_strings: result - ${palindromes}`);