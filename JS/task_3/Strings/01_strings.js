/*1. Write a JavaScript function reverses string and returns it
* Example: "sample" -> "elpmas".*/

const reverseString = (str) => {
    return str.split('').reverse().join('');
}
const str = 'sample';
console.log(`01_strings: source string: ${str}; function result: ${reverseString(str)}`);
