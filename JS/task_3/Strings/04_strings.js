/*You are given a text. Write a function that changes the text in all regions:
* `<upcase>text</upcase>` to uppercase.
* `<lowcase>text</lowcase>` to lowercase
* `<mixcase>text</mixcase>` to mix casing (random)*/

const changeString = (str) => {
    let strWithTags = str.matchAll(/<(?<tag>[\w]+)>(?<text>([\w\s]+)?([<[\w\s]+>]*)?([\w\s']+)*([<\/[\w\s]+>]*)?([\w\s]+)?)<\/[\w]+>/gi);
    let arrayWithTags = Array.from(strWithTags);

    for (const key of arrayWithTags) {
        let changedPart;
        if (key.groups.tag.toLowerCase() === 'upcase') {
            changedPart = key.groups.text.toUpperCase();
        } else if (key.groups.tag.toLowerCase() === 'lowcase') {
            changedPart = key.groups.text.toLowerCase();
        } else if (key.groups.tag.toLowerCase() === 'mixcase') {
            changedPart = key.groups.text.split('');
            for (let i = 0; i < changedPart.length; i++) {
                changedPart[i] = Math.round(Math.random()) === 0 ?
                    changedPart[i].toLowerCase() : changedPart[i].toUpperCase();
            }
            changedPart = changedPart.join('');
        }
        str = str.replace(key[0], changedPart);
    }
    return str;
}

const changeText = (str) => {
    str = changeString(str);
    if (str.search(/<(?<tag>[\w]+)>(?<text>[.\s\w\W]*?)<\/[\w]+>/gi) !== -1) {
        str = changeString(str);
    }
    return str;
}

let testString = 'We are <mixcase>living</mixcase> in<upcase> a <lowcase>yellow</lowcase> submarine</upcase>. We <mixcase>don\'t</mixcase> <lowcase><upcase>have</upcase> anything</lowcase> else.';
testString = changeText(testString);
console.log(`04_strings: result - ${testString}`);