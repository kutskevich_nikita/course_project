/*11. Write a function that formats a string using placeholders*/

const stringFormat = function () {
    let str = arguments[0];
    for (let i = 0; i < arguments.length - 1; i++) {
        str = str.replace(new RegExp(`[{]${i}[}]`, `gi`), arguments[i + 1]);
    }
    return str;
}

let format = "{0}, {1}, {0} text {2} {0} {1}, {2}{1}";
let strEleven = stringFormat(format, 1, "Pesho", "Gosho");
console.log(`11_strings: result - ${strEleven}`);