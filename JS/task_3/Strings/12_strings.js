/*12. Write a function that creates a HTML UL using a template for every HTML LI.
    The source of the list should an array of elements. Replace all placeholders marked with `-{...}-` with
    the value of the corresponding property of the object.*/

const generateList = (array, tmpl) => {
    let str = `<ul>`;
    for(const key in array){
        let formatTmpl = tmpl;
        formatTmpl = formatTmpl.replace(/-{name}-/g, array[key].name);
        formatTmpl = formatTmpl.replace(/-{age}-/g, array[key].age);
        str += `<li>${formatTmpl}</li>`;
    }
    str += '</ul>';
    return str;
}

const people = [{name: "Peter", age: 14}, {name: "John", age: 17}, {name: "Alex", age: 29}, {name: "Jane", age: 25}];
const template = document.getElementById("list-item").innerHTML;
const peopleList = generateList(people, template);
const divForTemplate = document.getElementById("list-item");
divForTemplate.innerHTML = peopleList;