/*6. Write a function that extracts the content of a html page given as text.
The function should return anything that is in a tag, without the tags*/

const getTextFromHtml = () => {
    return document.querySelector("html").textContent.replace(/\n/g,'');
}

const text = getTextFromHtml();
console.log(`06_strings: ${text}`);