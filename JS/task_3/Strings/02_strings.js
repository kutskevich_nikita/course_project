/*2. Write a JavaScript function to check if in a given expression the brackets are put correctly.
* Example of correct expression: `((a+b)/5-d)`.
* Example of incorrect expression: `)(a+b))`.*/

const checkBrackets = (str) => {
    let counter = 0;
    for(let key of str){
        if(key === '('){
            counter++;
        } else if(key === ')'){
            counter--;
        }
        if(counter < 0){
            return false;
        }
    }
    return counter === 0;
}

console.log(`02_strings: expression ((a+b)/5-d), result: ${checkBrackets('(a(+b)/5-d)') === true ? 'correct' : 'incorrect'}`)