/*Write a script that parses an URL address given in the format: `[protocol]://[server]/[resource]`
and extracts from it the `[protocol]`, `[server]` and `[resource]` elements. Return the elements in a JSON object.*/

const parseURL = (url) => {
    const regexp = /(?<protocol>[.\w]+):\/\/(?<server>[.\w]+)(?<resource>.+)/g;

    const result = url.matchAll(regexp);
    let obj;
    for (const key of result) {
        obj = JSON.stringify(key.groups);
    }
    return obj;
}

const url = `https://gitlab.com/kutskevich_nikita/course_project/-/merge_requests/4`;

const parsedUrl  = parseURL(url);
console.log(`07_strings: result - ${parsedUrl}`);
