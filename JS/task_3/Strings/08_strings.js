/*Write a JavaScript function that replaces in a HTML document given as string all the tags
    `<a href="...">...</a>` with corresponding tags `[URL=...]...[/URL]`. */

const replaceTags = (str) => {
    const result = str.matchAll(/<a[\w\s\d-='"/:]*href=['"](?<adress>[.\/\w\s:]+)['"][\w\s\d-='"/:]*>(?<text>[.\s\w]*?)<\/a>/g);

    for(const key of result){
        str = str.replace(key[0],`[URL=${key[1]}]${key[2]}[/URL]`)
    }
    return str;
}

let htmlString = '<p>Please visit <a text="fdsaf" href="http://academy.telerik.com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>';

htmlString = replaceTags(htmlString);
console.log(`08_string: result - ${htmlString}`);


