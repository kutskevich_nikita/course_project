/*Task 1
Create 3 variables x = 6, y = 15, and z = 4:
Perform and display the results of the following operations for these variables:
· x += y - x++ * z ;
· z = -- x - y * 5 ;
· y /= x + 5 % z ;
· z = x++ + y * 5 ;
· x = y - x++ * z ;
Note: for each operation please use default values: x = 6, y = 15, and z = 4*/

let x = 6, y = 15, z = 4;
console.log(`Task 1\n· ${x += y - x++ * z}`);
x = 6; y = 15; z = 4;
console.log(`· ${z = -- x - y * 5}`);
x = 6; y = 15; z = 4;
console.log(`· ${y /= x + 5 % z}`);
x = 6; y = 15; z = 4;
console.log(`· ${z = x++ + y * 5}`);
x = 6; y = 15; z = 4;
console.log(`· ${x = y - x++ * z}`);

/*Task 2
Calculate the arithmetic average value of the three integer values and display it on the screen.*/

x = 6;
console.log(`Task 2\nAverage value of the three integer values => ${(x + y + z) / 3}`);

/*Task 3
Write a program for calculating the volume - V and the surface area - S of the cylinder.
    The volume V of the cylinder of radius-r and height-h, is calculated by the formula: V = πr 2 h.
    The area S of the surface of the cylinder is calculated by the formula: S = 2π rh + 2π r 2 = 2π r (r + h).
    The results of calculations should be displayed on the screen.*/

const RADIUS = 2, HEIGHT = 4;
const volume = Math.PI * Math.pow(RADIUS,2) * HEIGHT;
const area = 2 * Math.PI * RADIUS * (RADIUS + HEIGHT);
console.log(`Task 3\nThe volume of the cylinder is ${volume}\nThe area of the cylinder is ${area}`);
