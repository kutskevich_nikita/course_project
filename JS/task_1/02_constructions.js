/*Task 1
Given two numbers A and B, where (A < B).
Display all integer numbers in interval from A to B (including borders).
Display all odd integer values located in the numerical interval from A to B (including borders).*/


const A = 1.6, B = 16.2;

const integerArray = [];
for (let i = Math.round(A); i <= Math.round(B); i++) {
    integerArray.push(i);
}
console.log(`Task 1\nAll integer numbers in interval from A to B (including borders) => ${integerArray}`);

const oddArray = [];
for (let i = Math.round(A); i <= Math.round(B); i++) {
    if (i % 2 !== 0) {
        oddArray.push(i);
    }
}
console.log(`All odd integer values located in the numerical interval from A to B (including borders) => ${oddArray}`);

/*Task 2
Write a program that will calculate the factorial of N, using the do-while loop.*/

let N = 10, factorial = 1;

do {
    factorial *= N--;
} while (N !== 0);
console.log(`Task 2\nFactorial of N => ${factorial}`);

/*Task 3
Using the loops, draw in the browser with spaces (& nbsp) and asterisks (*):
· Rectangle
· Right triangle
· Equilateral triangle
· Rhombus.*/

/*Rectangle*/
const rectangleHorizontal = 30;
const rectangleVertical = 20;
const star = '*';
const whitespace = ' &nbsp';
const divRectangle = document.getElementById('rectangle');


let rectangle = '';
for (let i = 0; i < rectangleVertical; i++) {
    if (i === 0 || i === rectangleVertical - 1) {
        for (let j = 0; j < rectangleHorizontal; j++) {
            rectangle += star;
        }
    } else {
        rectangle += star;
        for (let j = 0; j < rectangleHorizontal - 2; j++) {
            rectangle += whitespace;
        }
        rectangle += star;
    }
    rectangle += '<br>';
}

divRectangle.innerHTML = rectangle;

/*Right triangle*/

const rightTriangleCount = 30;
const divRightTriangle = document.getElementById('rightTriangle');

let rightTriangle = '';
for (let i = 0; i < rightTriangleCount; i++) {
    if (i === rightTriangleCount - 1) {
        for (let j = 0; j < rightTriangleCount; j++) {
            rightTriangle += star;
        }
    } else {
        for (let j = 0; j < rightTriangleCount; j++) {
            if (j === 0 || j === i) {
                rightTriangle += star;
            } else {
                rightTriangle += whitespace;
            }
        }
    }
    rightTriangle += '<br>';
}
divRightTriangle.innerHTML = rightTriangle;

/*Equilateral triangle*/

const divEquilateralTriangle = document.getElementById('equilateralTriangle');
const equilateralTriangleHeight = 20;
let equilateralTriangle = '';

for (let i = 0; i < equilateralTriangleHeight; i++) {
    for (let j = 0; j < equilateralTriangleHeight - i; j++) {
        equilateralTriangle += whitespace;
    }
    for (let j = 0; j < i * 2 + 1; j++) {
        equilateralTriangle += star;
    }
    equilateralTriangle += '<br>';
}
divEquilateralTriangle.innerHTML = equilateralTriangle;

/*Rhombus*/

const divRhombus = document.getElementById('rhombus');
const rhombusHeight = 30;
let rhombus = '';

const center = rhombusHeight / 2;
for (let i = 0; i < rhombusHeight; i++) {
    for (let j = 0; j < rhombusHeight; j++) {
        if (i < center) {
            if (j >= center - i && j <= center + i) {
                rhombus += star;
            } else {
                rhombus += whitespace;
            }
        } else {
            if (j > center + i - rhombusHeight + 1 && j < center - i + rhombusHeight - 1) {
                rhombus += star;
            } else {
                rhombus += whitespace;
            }
        }
    }
    rhombus += '<br>';
}
divRhombus.innerHTML = rhombus;