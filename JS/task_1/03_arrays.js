/*Task 1
Create an array of N elements, populate it with arbitrary integer values.
Output the largest value of the array, the smallest value of the array,
the total sum of the elements,
the arithmetic mean of all elements, output all odd values.*/

const arrayOfNumbers = [];
const N = 10;
for(let i = 0; i < N; i++){
    arrayOfNumbers[i] = Math.floor(Math.random() * (100-1)) + 1;
}

const sumOfElements = arrayOfNumbers.reduce((sum, item) => sum + item, 0);
const average = sumOfElements / arrayOfNumbers.length;
const oddArray = arrayOfNumbers.filter((item) => item % 2 !==0);

console.log(`Task 1\n`)
console.log(`Array of numbers: ${arrayOfNumbers.join(', ')}`);
console.log(`Largest value in the array: ${Math.max.apply(null, arrayOfNumbers)}`);
console.log(`Smallest value in the array: ${Math.min.apply(null, arrayOfNumbers)}`);
console.log(`Sum of elements: ${sumOfElements}`);
console.log(`Average: ${average}`);
console.log(`All odd numbers: ${oddArray.join(', ')}`);


/*Tasks 2
Create a two-dimensional array of 5x5 elements and populate it with
arbitrary integer values. On the main diagonal, replace all numbers
with the sign (-) by 10, and the numbers with the sign (+) by the number 20.*/

const twoDimensionalArray = [];
const n = 5;
for(let i = 0; i < n; i++){
    twoDimensionalArray[i] = [];
    for(let j = 0; j < n; j++){
        twoDimensionalArray[i][j] = Math.floor(Math.random() * 20 - 10);
    }
}

console.log(`\nTask 2\n`)
console.log('Initial array:');
console.log(twoDimensionalArray);

for(let i = 0; i < n; i++){
    twoDimensionalArray[i][i] < 0
        ? twoDimensionalArray[i][i] = 10
        : twoDimensionalArray[i][i] = 20;
}

console.log('Transformed array:');
console.log(twoDimensionalArray);