/*2. Make this syntax possible: let a = add(2)(3); //5*/

const add = (firstNum) =>{
    return function(secondNum){
        return firstNum + secondNum;
    }
}

const result = add(2)(3);
console.log(`02_closure: ${result}`);
