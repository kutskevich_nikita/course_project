/*1. Closures and Scopes. Сreate a function, which returns an array of functions, which return their index in the array.
    For better understanding, here is an example:
    let callbacks = createFunctions(5); // create an array, containing 5 functions

callbacks[0](); // must return 0
callbacks[3](); // must return 3*/

const createFunctions = (num) => {
    const arrayOfFunctions = new Array(num);

    for (let i = 0; i < arrayOfFunctions.length; i++){
      arrayOfFunctions[i] = () => i;
    }
    return arrayOfFunctions;
}

const callbacks = createFunctions(4);
console.log(`01_closure:`);
console.log(callbacks[0]());
console.log(callbacks[3]());