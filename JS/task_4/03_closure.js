/*3. Implement a function "createCounter(initValue: number)" which works in next way:

    let counter = createCounter();
console.log(counter()); // 0
console.log(counter()); // 1
console.log(counter()); // 2
--------------------------------------
    let counter = createCounter(13);
console.log(counter()); // 13
console.log(counter()); // 14
console.log(counter()); // 15*/

const createCounter = (num = 0) => {
    return function (){
        return num ++;
    }
}
console.log(`03_closure:`)
const firstCounter = createCounter();
console.log(firstCounter());
console.log(firstCounter());
console.log(firstCounter());

const secondCounter = createCounter(13);
console.log(secondCounter());
console.log(secondCounter());
console.log(secondCounter());