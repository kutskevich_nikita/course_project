/*4. Implement a function "createStorage()" which works in next way:

    let storage = createStorage();

storage.setItem('userName': 'Alex');
storage.getItem('userName'); // 'Alex'
storage.getItem('userAge'); // null
storage.hasItem('userAge'); // false
storage.setItem('userAge': 28);
storage.hasItem('userAge'); // true
storage.length(); // 2
storage.removeItem('userName');
storage.length(); // 1
storage.clear();
storage.length(); // 0*/

const createStorage = () => {
    return {
        store: {},
        setItem: function (itemName, item) {
            this.store[itemName] = item;
        },
        getItem: function (itemName) {
            return this.store.hasOwnProperty(itemName) ? this.store[itemName] : null;
        },
        hasItem: function (itemName) {
            return this.store.hasOwnProperty(itemName);
        },
        length: function () {
            return Object.keys(this.store).length;
        },
        removeItem: function (itemName) {
            delete this.store[itemName];
        },
        clear: function () {
            this.store = {};
        }
    };
}

const storage = createStorage();

console.log(`04_closure`); // 'Alex'

storage.setItem('userName', 'Alex');
console.log(storage.getItem('userName')); // 'Alex'
console.log(storage.getItem('userAge')); // null
console.log(storage.hasItem('userAge')); // false
storage.setItem('userAge', 28);
console.log(storage.hasItem('userAge')); // true
console.log(storage.length()); // 2
storage.removeItem('userName');
console.log(storage.length()); // 1
storage.clear();
console.log(storage.length()); // 0


