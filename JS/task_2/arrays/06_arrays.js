/*6. Write a program that finds the most frequent number in an array.
    If there are more than one, return the first.*/

const sixthArray = [1, 4, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3];

const findMostFrequent = (array) => {
    let maxCount = 1;
    let elementIndex = 0;
    for(let i = 0; i < array.length; i++){
        let count = 1;
        for(let j = i + 1; j < array.length; j++){
            if(array[i] === array[j]){
                count++;
            }
        }
        if(count > maxCount){
            maxCount = count;
            elementIndex = i;
        }
    }
    return [elementIndex, maxCount];
}

let [index, count] = findMostFrequent(sixthArray);
console.log(`06_arrays: ${sixthArray[index]} (${count} times)`);
