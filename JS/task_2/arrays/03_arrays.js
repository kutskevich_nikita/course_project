/*3. Write a script that finds the maximal sequence of equal elements
in an array. If there are more than one, return the first.*/

const thirdArray = [2, 1, 1, 2, 3, 3, 3, 3, 2, 2, 2, 2, 1];

const findEqualElements = (array) => {
    let position = 0;
    let numOfElements = 1;
    let maxSequence = 0;
    for (let i = 0; i < array.length - 1; i++){
        if(array[i] === array[i+1]){
            numOfElements++;
            if(maxSequence < numOfElements){
                maxSequence = numOfElements;
                position = i + 1;
            }
        } else {
            numOfElements = 1;
        }
    }
    return array.slice(position - maxSequence + 1, position + 1);
}

const arrayOfEqualElements = findEqualElements(thirdArray);
console.log(`03_arrays: ${arrayOfEqualElements}`);