/*2. Write a script that compares two char arrays
lexicographically (letter by letter).*/

const charArrayOne = [13,5,6];
const charArrayTwo = [1,3,5,'a',6];

const compareArraysLexicographically = (firstArray, secondArray) => {
    let result;
    const firstString = firstArray.join('');
    const secondString = secondArray.join('');

    switch(firstString.localeCompare(secondString)){
        case 1:
            result = 'первый массив больше';
            break;
        case -1:
            result = 'второй массив больше';
            break;
        case 0:
            result = 'массивы равны';
            break;
        default:
            break;
    }
    console.log(`02_arrays: ${result}`);
}

compareArraysLexicographically(charArrayOne, charArrayTwo);

