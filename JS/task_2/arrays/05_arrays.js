/*5. Sorting an array means to arrange its elements in increasing order.
    Write a script to sort an array.
    Use the **"selection sort"** algorithm: Find the smallest element,
    \move it at the first position, find the smallest from the rest,
    move it at the second position, etc. Hint: Use a second array*/

const fifthArray = [2, 6, 4, 1, 2, 32, 7, 23];

const sort = (array) => {
    for(let min = 0; min < array.length - 1; min++){
        let smallestElementIndex = min;
        for(let i = min + 1; i < array.length; i++){
            if(array[i] < array[smallestElementIndex]){
                smallestElementIndex = i;
            }
        }
        let temp = array[smallestElementIndex];
        array[smallestElementIndex] = array[min];
        array[min] = temp;
    }
}

sort(fifthArray);
console.log(`05_arrays: ${fifthArray}`);