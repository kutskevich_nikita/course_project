/*7. Write a program that finds the index of given element in a
sorted array of integers by using the binary search algorithm.*/

const seventhArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const binaryIndexSearch = (value, array) => {
    let firstIndex = 0;
    let lastIndex = array.length - 1;
    let positionIndex = -1;
    let middleIndex;
    let found = false;

    while(!found && firstIndex <= lastIndex){
        middleIndex = Math.floor((lastIndex + firstIndex) / 2);
        if(array[middleIndex] === value){
            found = true;
            positionIndex = middleIndex;
        } else if (array[middleIndex] > value){
            lastIndex = middleIndex - 1;
        } else {
            firstIndex = middleIndex + 1;
        }
    }
    return positionIndex;
}

let valueIndex = binaryIndexSearch(7, seventhArray);
console.log(`07_arrays: the index of the value is ${valueIndex}`);