/*4. Write a script that finds the maximal increasing sequence
in an array. If there are more than one, return the first.*/

const fourthArray = [3, 2, 3, 4, 5, 2, 5, 3, 4, 5, 6];

const findIncreasingSequence = (array) => {
    let numOfElements = 1;
    let position = 0;
    let maxSequence = 0;
    for(let i = 0; i < array.length - 1; i++){
        if(array[i + 1] - array[i] === 1){
            numOfElements++;
            if(maxSequence < numOfElements){
                maxSequence = numOfElements;
                position = i + 1;
            }
        } else {
            numOfElements = 1;
        }
    }
    return array.slice(position - maxSequence + 1, position + 1);
}

const arrayOfIncreasingElements = findIncreasingSequence(fourthArray);
console.log(`04_arrays: ${arrayOfIncreasingElements}`);