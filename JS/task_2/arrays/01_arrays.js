/*1. Write a script that allocates array of 20 integers and
initializes each element by its index multiplied by 5.
Print the obtained array on the console.*/

const array = new Array(20).fill(0).map((_, index) => index * 5);

console.log(`01_arrays: ${array}`);