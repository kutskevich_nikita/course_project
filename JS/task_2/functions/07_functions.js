/*7. Write a Function that returns the index of the first element in array
that is bigger than its neighbors, or -1, if there’s no such element.
    Use the function from the previous exercise.*/

const getIndexIsBigger = (array) => {
    for(let i = 1; i < array.length - 1; i++){
        const result = checkIsBigger(i, array);
        if(result === 1){
            return i;
        }
    }
    return -1;
}

const resultIndex = getIndexIsBigger(testArray);
console.log(`07_functions: Test array: ${testArray}; Index: ${resultIndex}`);