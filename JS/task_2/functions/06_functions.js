/*6. Write a function that checks if the element at given position in given array
of integers is bigger than its two neighbors (when such exist).*/

const checkIsBigger = (index, array) => {
    if(index === 0 || index === array.length - 1){
        return -1;
    } else if(array[index] > array[index - 1] &&
                array[index] > array[index + 1]){
        return 1;
    } else {
        return -1;
    }
}

const numOfElements = 10;
const testArray = [];
const index = 4;
for(let i = 0; i < numOfElements; i++){
    testArray[i] = Math.floor(Math.random() * 10);
}

const result = checkIsBigger(index, testArray);

console.log(`06_functions: Test array: ${testArray}; Index: ${index}; Result: ${result === -1 ? 'less': 'bigger'}`);
