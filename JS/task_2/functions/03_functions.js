/*3. Write a function that finds all the occurrences of word in a text
* The search can be case sensitive or case insensitive
* Use function overloading*/

const word = 'abba';
const text = 'bbcd aba abba bdu abba';

const findAllOccurrences = (str, value) => {
    let quantity = 0;
    let positions = [];
    for(let i = 0; i <= str.length - value.length; i++){
        if(str[i] === value[0]){
            for(let j = i + 1, k = 1; k < value.length; j++, k++){
                if(str[j] === value[k]){
                    if(k === value.length - 1){
                        quantity++;
                        positions.push(i);
                    }
                } else {
                    break;
                }
            }
        }
    }
    return [positions, quantity];
}

let [positions, count] = findAllOccurrences(text, word);
console.log(`03_functions: the word occurs ${count} times in index positions ${positions}`);