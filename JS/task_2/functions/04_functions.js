/*4. Write a function to count the number of divs on the web page*/

const countDivsOnPage = () =>{
    const array = document.getElementsByTagName('div');
    return array.length;
}

const divsCount = countDivsOnPage();
console.log(`04_functions: the number of divs on the web page is ${divsCount}`);