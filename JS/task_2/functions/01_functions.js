/*1. Write a function that returns the last digit of given integer
as an English word. Examples: 512 -> "two", 1024 -> "four", 12309 -> "nine"*/

const returnLastDigit = (value) => {
    const lastDigit = value % 10;
    const wordsArray = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    return wordsArray[lastDigit];
}

const lastDigitAsWord = returnLastDigit(512);
console.log(`01_functions: ${lastDigitAsWord}`);
