/*Write a function that reverses the digits of
given decimal number. Example: 256 -> 652*/

const number = 765;

const reverseDigits = (value) => {
    let reversedValue = 0;
    while(value){
        reversedValue *= 10;
        reversedValue += value % 10;
        value = Math.floor(value / 10);
    }
    return reversedValue;
}

const reversedNumber = reverseDigits(number);
console.log(`02_functions: ${reversedNumber}`);
