/*5. Write a function that counts how many times given number appears in given array.
    Write a test function to check if the function is working correctly.*/

const countNum = (value, array) => {
    let count = 0;
    array.forEach(current => {
            if (current === value) {
                count++;
            }
        }
    )
    return count;
}

const testCountNum = (numOfCalls) => {

    const testNumber = 15;

    for (let call = 0; call < numOfCalls; call++) {  /*calling the countNum() function numOfCalls times*/

        const numOfTestElements = Math.floor(Math.random() * 5 + 1)  /*the number of test items in the array*/
        let testArray = [];
        const numOfElements = numOfTestElements + Math.floor(Math.random() * 5 + 1);  /*generating the number of elements of the test array*/

        for (let i = 0; i < numOfElements; i++) {  /*test array generation*/
            testArray[i] = Math.floor(Math.random() * 10);
        }

        for (let i = 0; i < numOfTestElements;) {  /*inserting a test number into an array*/
            let index = Math.floor(Math.random() * testArray.length);
            if (testArray[index] !== testNumber) {
                testArray[index] = testNumber;
                i++;
            }
        }

        const testCount = countNum(testNumber, testArray);

        console.log(`Test array: ${testArray}; Test number: ${testNumber}; Function execution result: ${testCount}`);

        if(testCount !== numOfTestElements){
            throw new Error('the countNum() function does not work correctly');
        }
    }
}

console.log(`05_functions:`);
testCountNum(3);