class Person{

    #firstName = '';
    #lastName = '';
    #age = 0;

    constructor(firstName, lastName, age) {
        if(this._checkName(firstName)){
            this.#firstName = firstName;
        }
        if(this._checkName(lastName)){
            this.#lastName = lastName;
        }
        this.#age = this._checkAge(age);
    }

    _checkAge (value) {
        typeof value !== 'number' ? value = parseInt(value, 10): value;
        if(value >= 0 && value <= 150){
           return value;
        } else {
            throw new Error('Age must always be a number in the range (0, 150), inclusive');
        }
    }

    _checkName (str) {
        if(typeof str === 'string' && str.length >= 3 && str.length <= 20 && !!str.match(/^[a-z]+$/gi)) {
            return true;
        }  else {
            throw new Error('firstName must be a string between 3 and 20 characters and containing only Latin letters');
        }
    }

    get fullName () {
        return (`${this.#firstName} ${this.#lastName}`.toUpperCase());
    }
    set fullName (name) {
        const nameArray = name.match(/[\S]+/gi);
        if(this._checkName(nameArray[0])){
            this.#firstName = nameArray[0];
        }
        if(this._checkName(nameArray[1])){
            this.#lastName = nameArray[1];
        }
    }
    introduce () {
        return `Hello! My name is ${this.#firstName} ${this.#lastName} and I am ${this.#age} years old`
    }
}

const person = new Person('Max', 'Pupkin', 34);
person.fullName = 'Andrey Vezovik';
console.log(person.fullName);
console.log(person.introduce());

