function Person(firstName, lastName, age) {

    if (this._checkName(firstName)) {
        this._firstName = firstName;
    }
    if (this._checkName(lastName)) {
        this._lastName = lastName;
    }
    this._age = this._checkAge(age);

    Object.defineProperty(this, 'fullName', {
        get () {
            return (`${this._firstName} ${this._lastName}`.toUpperCase());
        },
        set (name) {
            const nameArray = name.match(/[\S]+/gi);
            if (this._checkName(nameArray[0])) {
                this._firstName = nameArray[0];
            }
            if (this._checkName(nameArray[1])) {
                this._lastName = nameArray[1];
            }
        }
    });
}

Person.prototype._checkAge = function (value) {
    typeof value !== 'number' ? value = parseInt(value, 10) : value;
    if (value >= 0 && value <= 150) {
        return value;
    } else {
        throw new Error('Age must always be a number in the range (0, 150), inclusive');
    }
}

Person.prototype._checkName = function (str) {
    if (typeof str === 'string' && str.length >= 3 && str.length <= 20 && !!str.match(/^[a-z]+$/gi)) {
        return true;
    } else {
        throw new Error('firstName must be a string between 3 and 20 characters and containing only Latin letters');
    }
}

Person.prototype.introduce = function () {
    return `Hello! My name is ${this._firstName} ${this._lastName} and I am ${this._age} years old`
}


const person = new Person('Max', 'Pupkin', 34);
person.fullName = 'Andrey Vezovik';
console.log(person.fullName);
console.log(person.introduce());


