/*Implement a function `instanceOf(inst, clazz)` which works like native `instanceof` operator (`inst instanceof clazz`).
    Example:
class A {}
class B extends A {}
class C extends A {}

let arr = [];

instanceOf(new B(), A); // true
instanceOf(new C(), A); // true
instanceOf(new B(), B); // true
instanceOf(new B(), C); // false
instanceOf(new C(), B); // false
instanceOf(arr, Object); // true

NOTE: using `instnceof` and `Object.prototype.isPrototypeOf` are forbidden.*/

const instanceOf = (inst, clazz) => {
    if (inst.__proto__ === null) return false;
    return inst.__proto__ === clazz.prototype ? true : instanceOf(inst.__proto__, clazz);
}

class A{}
class B extends A{}
class C extends A{}

let arr = [];

console.log(instanceOf(new B(), A));
console.log(instanceOf(new C(), A));
console.log(instanceOf(new B(), B));
console.log(instanceOf(new B(), C));
console.log(instanceOf(new C(), B));
console.log(instanceOf(arr, Object));
