/*Create the function "str" which takes one parameter and alert(“String is non empty”) if string is non empty
and alert(String is empty) otherwise. Use following funtion to check this condition.
    Create the function "isNonEpmtyStr" as a property of function "str".
    This function takes one parameter and return true if its parameter is NonEmptyStr.
    Test Data:
    str.isNonEmptyStr(), result = false
str.isNonEmptyStr(“”), result = false
str.isNonEmptyStr(“a”), result = true
str.isNonEmptyStr(1), result = false
str(), alert(“String is empty”)
str(“a”), alert(“String is non empty”)  */

const str = (param) => {
    alert(typeof (param) === "string" && !!param ? "String is not empty" : "String is empty");
}

str.isNonEmptyStr = (testStr) => {
    return typeof(testStr) === "string" && !!testStr;
}

console.log(`09_functions: `);
console.log(str.isNonEmptyStr());
console.log(str.isNonEmptyStr(''));
console.log(str.isNonEmptyStr('a'));
console.log(str.isNonEmptyStr(1));
str();
str('a');
