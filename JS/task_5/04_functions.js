/*Create function "fibo" to calculate fibonachi numbers using named function expression.*/

const fibo = function fiboCount(n){
    return n <= 1 ? n : fiboCount(n - 1) + fiboCount(n - 2);
}

const fiboResult = fibo(6); //8
console.log(`04_functions: ${fiboResult}`);