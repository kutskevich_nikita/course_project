/*Make the function "conc" (from task 1) immediately-invoked function expression  */

const result = (function (a, b) {
    return a + '' + b;
})(1, 1);

console.log(`05_functions: ${result}`);