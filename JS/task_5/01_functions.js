/*Create function "conc" which should concatenate two parameters a and b and return
concatanating string using Function Declaration Statement (FDS). Call this function before its declaration.
    Test Data: a = “1”, b = “1”, result = “11” a = 1, b = 1, result = “11”  */

const concResult = conc(1, 1);

console.log(`01_functions: ${concResult}`);

function conc(a, b) {
    return a + '' + b;
    //return a.toString() + b.toString(); // Second option
    //return `${a}${b}`; // Third option
 }
 