/*Create function "comp" which should compare two parameters a and b and return 1
if a equal b and -1 if a not equal b using Function Definition Expression (FDE).
    Test Data: a = “abc”, b = “abc”, result = 1 a = “abC”, b = “abc”, result = -1  */

const comp = function(a, b){
    return a.localeCompare(b) === 0 ? 1 : -1;
}

const compResult = comp("abc", "abC");
console.log(`02_functions: ${compResult}`);