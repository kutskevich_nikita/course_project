/*Create function "parts" which takes several parameters. Each parameter is a group of sentences.
This function should extract the substring from the sign “:”(colon) to the sign “.”(period) of each parameter and
return the array of this substrings Use Function Definition Expression.
    Test Data:
    param1 = "This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas."
param2 = "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black."
result = [“cherries, oranges, apples, bananas”, “red, blue, yellow, black”]
*/

const parts = function (...args){
    const array = [];
    for(let key of args){
        const substr = Array.from(key.toString().matchAll(/:[\s]([\w\s,]+)./g));
        for ( let elem of substr){
            array.push(elem[1]);
        }
    }
    return array;
}

const result = parts("This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.",
    "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.");

console.log(`06_functions: `);
console.log(result);
