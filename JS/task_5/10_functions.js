/*Function as a Parameter.
    Create the function "toConsole" with one parameter, which display the value of its parameter in console.log.
    Create the function "toAlert" with one parameter, which display the value of its parameter using alert().
    Create the function "splitToWords" with two parameters: msg and callback.
    This function splits msg into words and using callback to displays words in console or by alert.
    If the second parameter is omitted, return the array of words.
    Test Data:
    splitToWords("My very long text msg", toConsole);  result in console:  My very long text msg
splitToWords("My very long text msg", toAlert); result by alert: (My), (very) ….
console.log( splitToWords("My very long text msg") ); result: [“My”, “very”, “long”, “text”, “msg”]  */

const toConsole = (msg) => {
    console.log(msg);
}
const toAlert = (msg) => {
    alert(msg);
}

const splitToWords = (msg, callback) => {
   if(callback === undefined){
       return msg.split(' ');
   } else {
       callback(msg);
   }
}

console.log(`10_functions:`)
splitToWords("My very long text msg", toConsole);
splitToWords("My very long text msg", toAlert);
console.log(splitToWords("My very long text msg"));