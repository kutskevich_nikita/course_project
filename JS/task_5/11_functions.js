/*Function as a Result.
    Create function "copyright" which returns another function with one parameter.
    Returned function adds sign © (“\u00A9”) at the beginning of its parameter. Declare copyright sign in outer function.
Test Data: console.log( copyright()(“CLEVERTEC”) ); result = "© CLEVERTEC". */

const copyright = () => {
    const sign = '\u00A9';
    return (param) => {
        return sign.concat(' ',param);
    };
}

console.log(`11_functions: ${copyright()("CLEVERTEC")}`);
