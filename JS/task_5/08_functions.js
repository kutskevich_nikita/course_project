/*Optional Arguments.
    Create function find(testString, test) which should return the position of "test" string in "testString". If you don’t pass the second parameter use test = testString. Use Function Definition Expression.
    Test Data:
    testString = “abc”, test =”b”, result = 1
testString = “abc”, result = 0
testString = “abc”, test = “d”, result = -1
testString = “abc”, test=”a”, test2=”b”, result = 0 */

const find = function (testString, test = testString){
    return testString.indexOf(test);
}

const result = find('abc', 'b');
console.log(`08_functions: ${result}`);
