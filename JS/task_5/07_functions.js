/*Perform task 6 using the arrow functions*/

const parts = (...args) => {
    const array = [];
    for(let key of args){
        const substr = Array.from(key.toString().matchAll(/:[\s]([\w\s,]+)./g));
        for ( let elem of substr){
            array.push(elem[1]);
        }
    }
    return array;
}

const result = parts("This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.",
    "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.");

console.log(`07_functions: `);
console.log(result);